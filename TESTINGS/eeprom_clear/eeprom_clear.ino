#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "DHT.h"
#define DHTTYPE DHT11

#define GRID_PIN 2
#define TRIP_PIN 3
#define UPS_PIN 4
#define DHT11_SENSOR_PIN 5
#define DOOR_SENOSOR_PIN 6
#define SERIAL_TX 7
#define SERIAL_RX 8
#define MODBUS_DI 12
#define MODBUS_RO 13
#define LIGHT_1_PIN A0
#define LIGHT_2_PIN A1
#define LIGHT_3_PIN A2
#define LIGHT_4_PIN A3

#define START_CHAR String("!")
#define END_CHAR String("%")
/*#define ROOM_TEMP_BACK_KEY String("\"room_temp_back\": ")
#define B_DOOR_STATE_KEY String("\"b_door_state\": ")
#define B_DOOR_COUNT_KEY String("\"b_door_count\": ")*/
#define ROOM_TEMP_BACK_KEY String("\"rtb\": ")
#define B_DOOR_STATE_KEY String("\"bds\": ")
#define B_DOOR_COUNT_KEY String("\"bdc\": ")
#define L1_KEY String("\"l1\": ")
#define L2_KEY String("\"l2\": ")
#define L3_KEY String("\"l3\": ")
#define L4_KEY String("\"l4\": ")
#define GRID_KEY String("\"grid\": ")
#define TRIP_KEY String("\"trip\": ")
#define UPS_KEY String("\"ups\": ")

#define SEND_COUNT 20
#define SEND_DELAY 200

#define LIGHT_READ_AVERAGE_COUNT 5
#define LIGHT_SENSOR_THRESHOLD 500

#define B_DOOR_COUNT_EEPROM_ADDRESS 10

#define NEGLIGIBLE_TEMP_DIFF 0.25  //if the temperature difference between old and current are greater than this, push the new value
#define TEMP_READ_INTERVAL 5000 //temperature read interval in miliseconds

//SoftwareSerial mySerial(SERIAL_RX, SERIAL_TX); // RX, TX
SoftwareSerial mySerial(MODBUS_RO, MODBUS_DI); // RX, TX

DHT dht(DHT11_SENSOR_PIN, DHTTYPE);

bool read_grid_state;
bool read_trip_state;
bool read_ups_state;
bool read_b_door_state;
float read_temperature;
uint16_t b_door_count;
bool read_light_1;
bool read_light_2;
bool read_light_3;
bool read_light_4;

bool old_read_grid_state;
bool old_read_trip_state;
bool old_read_ups_state;
bool old_read_b_door_state;
float old_read_temperature;
bool old_read_light_1;
bool old_read_light_2;
bool old_read_light_3;
bool old_read_light_4;

bool need_to_send_temp_door = false;
bool need_to_send_light = false;
bool need_to_send_power = false;

unsigned long last_temp_read_millis;

void setup() {
  Serial.begin(115200);
  mySerial.begin(9600);
  dht.begin();

  pinMode(GRID_PIN, INPUT);
  pinMode(TRIP_PIN, INPUT);
  pinMode(UPS_PIN, INPUT);
  pinMode(DOOR_SENOSOR_PIN, INPUT); //pulldown to NO switch, when closed: read 1
  pinMode(LIGHT_1_PIN, INPUT);
  pinMode(LIGHT_2_PIN, INPUT);
  pinMode(LIGHT_3_PIN, INPUT);
  pinMode(LIGHT_4_PIN, INPUT);

  delay(6000);

  read_grid_state = digitalRead(GRID_PIN);
  read_trip_state = digitalRead(TRIP_PIN);
  read_ups_state = digitalRead(UPS_PIN);
  read_b_door_state = (bool)(!digitalRead(DOOR_SENOSOR_PIN)); //inverted the signal to read 1 when door is open
  EEPROM.put(B_DOOR_COUNT_EEPROM_ADDRESS, 0); //get backdoor value from EEPROM
}

void loop() {
  Serial.print("analog 1: ");
  Serial.println(analogRead(LIGHT_1_PIN));
  Serial.print("analog 2: ");
  Serial.println(analogRead(LIGHT_2_PIN));
  Serial.print("analog 3: ");
  Serial.println(analogRead(LIGHT_3_PIN));
  Serial.print("analog 4: ");
  Serial.println(analogRead(LIGHT_4_PIN));
  delay(1000); //loop delay
}

bool isLightOn(int pin)
{
  int analog_val = 0;
  for (int i = 0; i < LIGHT_READ_AVERAGE_COUNT; i++)
  {
    analog_val += analogRead(pin);
    delay(10);
  }
  analog_val = (int)(analog_val / LIGHT_READ_AVERAGE_COUNT); //taking the average

  if (analog_val >= LIGHT_SENSOR_THRESHOLD)
  {
    return true;
  }
  else
  {
    return false;
  }
}
