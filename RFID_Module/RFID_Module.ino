#include <SPI.h>
#include <MFRC522.h>
//#include <SoftwareSerial.h>
#include<avr/wdt.h>

//SoftwareSerial mySerial(4, 3); // RX, TX

#define SS_PIN 10
#define RST_PIN 9
#define DUMMY_RST_PIN A0
MFRC522 mfrc522(SS_PIN, DUMMY_RST_PIN);

String content = "";
String rfid_string = "";

#define sig 2
#define BUZZER_PIN 5

bool is_inside = false;
void setup()
{
  Serial.begin(115200);
  //Serial.println("Reset");
  //mySerial.begin(9600);
  SPI.begin();
  mfrc522.PCD_Init();
  //Serial.println("Approximate your card to the reader...");
  //Serial.println();
  pinMode(sig, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  digitalWrite(sig, LOW);
  
  digitalWrite(BUZZER_PIN, LOW);
  
  wdt_disable();  /* Disable the watchdog and wait for more than 2 seconds */
  delay(3000);  /* Done so that the Arduino doesn't keep resetting infinitely in case of wrong configuration */
  wdt_enable(WDTO_8S);  /* Enable the watchdog with a timeout of 10 seconds */
  
  Serial.println("! RFID @");
}
void loop()
{
  //Serial.println("Loop");
  delay(1000);

  wdt_reset();
  if ( !(mfrc522.PICC_IsNewCardPresent()))
  {
    return;
  }

  if ( ! (mfrc522.PICC_ReadCardSerial()))
  {
    return;
  }

  //Serial.print("UID tag :");
  content = "";
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    //Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    //Serial.print(mfrc522.uid.uidByte[i], HEX);
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  //Serial.println();
  //Serial.print("Message : ");
  content.toUpperCase();

  rfid_string = "!" + content.substring(1) + "@";
  Serial.println(rfid_string);

  digitalWrite(sig, HIGH);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(250);
  digitalWrite(sig, LOW);
  digitalWrite(BUZZER_PIN, LOW);
  delay(200); 
  digitalWrite(sig, HIGH);
  digitalWrite(BUZZER_PIN, HIGH);
  delay(1000);
  digitalWrite(sig, LOW);
  digitalWrite(BUZZER_PIN, LOW);

  /*if (content.substring(1) == "53 62 21 17") // Make sure you change this with your own UID number
    {
    //Serial.println("Authorised access");
    //Serial.println();
    //delay(3000);

    is_inside = !(is_inside);
    }

    else   {
    //Serial.println(" Access denied");
    //delay(3000);
    }*/
}
