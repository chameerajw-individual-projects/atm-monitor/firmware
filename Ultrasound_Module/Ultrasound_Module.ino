#include <ArduinoSort.h>
#include <SoftwareSerial.h>

#define TRIG_PIN_2 4
#define ECHO_PIN_2 5

/*#define TRIG_PIN_3 6
#define ECHO_PIN_3 7
#define TRIG_PIN_4 8
#define ECHO_PIN_4 9*/
#define TRIG_PIN_3 8
#define ECHO_PIN_3 9

#define TRIG_PIN_4 6
#define ECHO_PIN_4 7

#define TRIG_PIN_5 10
#define ECHO_PIN_5 11
#define TRIG_PIN_6 12
#define ECHO_PIN_6 13

#define SIG_PIN A0

#define MODBUS_DI 2
#define MODBUS_RO 3

#define SOUND_SPEED 500.0 //speed of sound in ms-1
#define AVERAGE_COUNT 1

#define PERSON_IN_RANGE 100.0 //100.0 //if the read distance is less than this, a person is inside
#define ALLOWED_IN_DURATION_MIN 15 //15 

#define START_CHAR String("!")
#define END_CHAR String("%")
#define PERSON_MOVE_KEY String("\"pm\": ")
#define IN_TIME_KEY String("\"it\": ")

#define SEND_COUNT 20
#define SEND_DELAY 200

SoftwareSerial mySerial(MODBUS_RO, MODBUS_DI); // RX, TX

bool is_inside = false;
bool old_is_inside = false;

long person_in_Start_millis;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  mySerial.begin(9600);

  pinMode(TRIG_PIN_2, OUTPUT);
  pinMode(TRIG_PIN_3, OUTPUT);
  pinMode(TRIG_PIN_4, OUTPUT);
  pinMode(TRIG_PIN_5, OUTPUT);
  pinMode(TRIG_PIN_6, OUTPUT);

  pinMode(ECHO_PIN_2, INPUT);
  pinMode(ECHO_PIN_3, INPUT);
  pinMode(ECHO_PIN_4, INPUT);
  pinMode(ECHO_PIN_5, INPUT);
  pinMode(ECHO_PIN_6, INPUT);

  pinMode(SIG_PIN, OUTPUT);

  //delay(12000);
}

void loop() {
    Serial.print("SENSOR 3:");
    Serial.println(readUltra3());
    delay(1000);
}

float readUltra2() {
  delay(500);
  float read_distance_array[AVERAGE_COUNT] = {};
  for (int count = 0; count < AVERAGE_COUNT; count++) {
    float read_distance;
    double read_duration;
    digitalWrite(TRIG_PIN_2, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_PIN_2, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG_PIN_2, LOW);
    read_duration = pulseIn(ECHO_PIN_2, HIGH, 26000);
    read_distance = (read_duration * SOUND_SPEED / 2) / 10e3; //10e4 = 10e6(microsec to sec) / 10e2(m to cm)
    //Serial.print(read_distance);
    //Serial.print(", ");
    read_distance_array[count] = read_distance;
    delay(20);
  }
  sortArray(read_distance_array, AVERAGE_COUNT);
  return max(25.00, read_distance_array[(int)(AVERAGE_COUNT / 2)]);
}

float readUltra3() {
  //delay(500);
  float read_distance_array[AVERAGE_COUNT] = {};
  for (int count = 0; count < AVERAGE_COUNT; count++) {
    float read_distance;
    float read_duration;
    digitalWrite(TRIG_PIN_3, LOW);
    delayMicroseconds(2);
    digitalWrite(TRIG_PIN_3, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG_PIN_3, LOW);
    read_duration = float(pulseIn(ECHO_PIN_3, HIGH));
    Serial.println(read_duration);
    read_distance = (read_duration * 0.034 / 2); //10e4 = 10e6(microsec to sec) / 10e2(m to cm)
    //Serial.print(read_distance);
    //Serial.print(", ");
    return read_distance;
    read_distance_array[count] = read_distance;
    delay(100);
  }
  sortArray(read_distance_array, AVERAGE_COUNT);
  return max(25.00, read_distance_array[(int)(AVERAGE_COUNT / 2)]);
}

float readUltra4() {
  delay(500);
  float read_distance_array[AVERAGE_COUNT] = {};
  for (int count = 0; count < AVERAGE_COUNT; count++) {
    float read_distance;
    double read_duration;
    digitalWrite(TRIG_PIN_4, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_PIN_4, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG_PIN_4, LOW);
    read_duration = pulseIn(ECHO_PIN_4, HIGH, 26000);
    read_distance = (read_duration * SOUND_SPEED / 2) / 10e3; //10e4 = 10e6(microsec to sec) / 10e2(m to cm)
    //Serial.print(read_distance);
    //Serial.print(", ");
    read_distance_array[count] = read_distance;
    delay(20);
  }
  sortArray(read_distance_array, AVERAGE_COUNT);
  return max(25.00, read_distance_array[(int)(AVERAGE_COUNT / 2)]);
}

float readUltra5() {
  delay(500);
  float read_distance_array[AVERAGE_COUNT] = {};
  for (int count = 0; count < AVERAGE_COUNT; count++) {
    float read_distance;
    double read_duration;
    digitalWrite(TRIG_PIN_5, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_PIN_5, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG_PIN_5, LOW);
    read_duration = pulseIn(ECHO_PIN_5, HIGH, 26000);
    read_distance = (read_duration * SOUND_SPEED / 2) / 10e3; //10e4 = 10e6(microsec to sec) / 10e2(m to cm)
    //Serial.print(read_distance);
    //Serial.print(", ");
    read_distance_array[count] = read_distance;
    delay(20);
  }
  sortArray(read_distance_array, AVERAGE_COUNT);
  return max(25.00, read_distance_array[(int)(AVERAGE_COUNT / 2)]);
}

float readUltra6() {
  delay(500);
  float read_distance_array[AVERAGE_COUNT] = {};
  for (int count = 0; count < AVERAGE_COUNT; count++) {
    float read_distance;
    double read_duration;
    digitalWrite(TRIG_PIN_6, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_PIN_6, HIGH);
    delayMicroseconds(20);
    digitalWrite(TRIG_PIN_6, LOW);
    read_duration = pulseIn(ECHO_PIN_6, HIGH, 26000);
    read_distance = (read_duration * SOUND_SPEED / 2) / 10e3; //10e4 = 10e6(microsec to sec) / 10e2(m to cm)
    //Serial.print(read_distance);
    //Serial.print(", ");
    read_distance_array[count] = read_distance;
    delay(20);
  }
  sortArray(read_distance_array, AVERAGE_COUNT);
  return max(25.00, read_distance_array[(int)(AVERAGE_COUNT / 2)]);
}
