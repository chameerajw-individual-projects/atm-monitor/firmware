
#include <SoftwareSerial.h>
#include "DHT.h"
#define DHTTYPE DHT22
//#define DHTTYPE DHT11

#define DHT22_SENSOR_PIN 3
#define DOOR_SENOSOR_PIN 4
#define MODBUS_DI 9
#define MODBUS_RO 7

#define START_CHAR String("!")
#define END_CHAR String("%")
//#define ROOM_TEMP_FRONT_KEY String("\"room_temp_front\": ")
//#define F_DOOR_STATE_KEY String("\"f_door_state\": ")
#define ROOM_TEMP_FRONT_KEY String("\"rtf\": ")
#define F_DOOR_STATE_KEY String("\"fds\": ")

#define SEND_COUNT 20
#define SEND_DELAY 200

#define NEGLIGIBLE_TEMP_DIFF 1.00  //if the temperature difference between old and current are greater than this, push the new value

#define TEMP_READ_INTERVAL 5000 //temperature read interval in miliseconds

SoftwareSerial mySerial(MODBUS_RO, MODBUS_DI); // RX, TX

DHT dht(DHT22_SENSOR_PIN, DHTTYPE);

float read_temperature;
volatile bool read_f_door_state;

float old_read_temperature;
bool old_read_f_door_state;

bool need_to_send = false;

unsigned long last_temp_read_millis;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  mySerial.begin(9600);
  dht.begin();

  pinMode(DOOR_SENOSOR_PIN, INPUT_PULLUP);
  
  //delay(9000);

  read_temperature = dht.readTemperature();
  last_temp_read_millis = millis();
  old_read_temperature = read_temperature;

  read_f_door_state = (bool)digitalRead(DOOR_SENOSOR_PIN);
  old_read_f_door_state = read_f_door_state;

  need_to_send = true; //send for the first time
}

void loop() {
  if ((millis() - last_temp_read_millis) > TEMP_READ_INTERVAL)
  {
    read_temperature = dht.readTemperature();
    last_temp_read_millis = millis();

    if (abs(read_temperature - old_read_temperature) >= NEGLIGIBLE_TEMP_DIFF)
    {
      need_to_send = true; //temperature difference detected
      old_read_temperature = read_temperature;
    }
  }

  read_f_door_state = (bool)digitalRead(DOOR_SENOSOR_PIN);
  if (read_f_door_state != old_read_f_door_state)
  {
    need_to_send = true; //door movement detected
    old_read_f_door_state = read_f_door_state;
  }

  if (need_to_send == true)
  {
    String send_string = START_CHAR + ROOM_TEMP_FRONT_KEY + String(read_temperature, 2) + ", " + F_DOOR_STATE_KEY +  String((int)read_f_door_state) + END_CHAR;
    Serial.println(send_string);
    for (int i = 0; i < SEND_COUNT; i++)
    {
      mySerial.println(send_string);
      delay(SEND_DELAY);
    }
    need_to_send = false; //reset sent flag
  }
}
