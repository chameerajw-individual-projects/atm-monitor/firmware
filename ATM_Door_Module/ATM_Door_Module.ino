#include <SoftwareSerial.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define TEMP_SENSOR_PIN 4
#define VIB_SENOSOR_PIN 3
#define MODBUS_DI 6
#define MODBUS_RO 7

#define START_CHAR String("!")
#define END_CHAR String("%")
//#define ATM_TEMP_KEY String("\"atm_temp\": ")
//#define ATM_VIBR_KEY String("\"atm_vibr\": ")
#define ATM_TEMP_KEY String("\"atmt\": ")
#define ATM_VIBR_KEY String("\"atmv\": ")

#define SEND_COUNT 20
#define SEND_DELAY 200

#define NEGLIGIBLE_TEMP_DIFF 1.00 //if the temperature difference between old and current are greater than this, push the new value

#define TEMP_READ_INTERVAL 5000 //temperature read interval in miliseconds

SoftwareSerial mySerial(MODBUS_RO, MODBUS_DI); // RX, TX

OneWire oneWire(TEMP_SENSOR_PIN);
DallasTemperature tempSensors(&oneWire);

float read_temperature;
volatile bool read_vibration;

float old_read_temperature;
bool old_read_vibration;

bool need_to_send = false;

unsigned long last_temp_read_millis;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  mySerial.begin(9600);
  tempSensors.begin();
  delay(3000);
  
  pinMode(VIB_SENOSOR_PIN, INPUT);

  tempSensors.requestTemperatures();
  read_temperature = tempSensors.getTempCByIndex(0);
  last_temp_read_millis = millis();
  old_read_temperature = read_temperature;

  read_vibration = (bool)digitalRead(VIB_SENOSOR_PIN);
  old_read_vibration = read_vibration;

  need_to_send = true; //send for the first time
}

void loop() {
  if ((millis() - last_temp_read_millis) > TEMP_READ_INTERVAL)
  {
    tempSensors.requestTemperatures();
    read_temperature = tempSensors.getTempCByIndex(0);
    last_temp_read_millis = millis();

    if (abs(read_temperature - old_read_temperature) >= NEGLIGIBLE_TEMP_DIFF)
    {
      need_to_send = true; //temperature difference detected
      old_read_temperature = read_temperature;
    }
  }

  read_vibration = (bool)digitalRead(VIB_SENOSOR_PIN);
  if (read_vibration != old_read_vibration)
  {
    //read_vibration change
    need_to_send = true; //vibration detection change
    old_read_vibration = read_vibration;
  }

  if (need_to_send == true)
  {
    String send_string = START_CHAR + ATM_TEMP_KEY + String(read_temperature, 2) + ", " + ATM_VIBR_KEY +  String((int)read_vibration) + END_CHAR;
    Serial.println(send_string);
    for (int i = 0; i < SEND_COUNT; i++)
    {
      mySerial.println(send_string);
      delay(SEND_DELAY);
    }
    need_to_send = false; //reset sent flag
  }

  delay(500); //loop delay
}
