#include <SoftwareSerial.h>

#define F_ROOM_TEMP_RX 2
#define F_ROOM_TEMP_TX 3
#define ATM_DOOR_RX 4
#define ATM_DOOR_TX 5
#define ULTRASOUND_RX 6
#define ULTRASOUND_TX 7
#define RFID_RX 8
#define RFID_TX 9
#define SENSORS_RX 12
#define SENSORS_TX 13


#define SLAVE_START_CHAR String("!")
#define SLAVE_END_CHAR String("%")
#define START_CHAR String("{")
#define END_CHAR String("}")
#define TERMINATE_CHAR String(", ")

#define DELAY_PER_SLAVE 500 //sampling period per serial receiver

#define MAX_UNSEND_DURATION 3 //send atleast an empty packet for 3 min

SoftwareSerial fRoomSerial(F_ROOM_TEMP_RX, F_ROOM_TEMP_TX); // RX, TX
SoftwareSerial atmSerial(ATM_DOOR_RX, ATM_DOOR_TX); // RX, TX
SoftwareSerial ultrasoundSerial(ULTRASOUND_RX, ULTRASOUND_TX); // RX, TX
//SoftwareSerial sensorSerial(SENSORS_RX, SENSORS_TX); // RX, TX
SoftwareSerial sensorSerial(RFID_RX, RFID_TX); // RX, TX

bool is_data_available = false;

unsigned long last_sent_millis = 0;

void setup() {
  Serial.begin(115200);

  fRoomSerial.begin(9600);
  atmSerial.begin(9600);
  ultrasoundSerial.begin(9600);
  sensorSerial.begin(9600);

  last_sent_millis = millis();
}

void loop() {
  String send_string = START_CHAR;
  
  double min_from_last_sent = (double)(millis() - last_sent_millis) / (double)(60000);
  if((int)min_from_last_sent >= MAX_UNSEND_DURATION)
  {
    is_data_available = true;
  }
  
  /************************* F_ROOM_TEMP_MODULE ************************/
  fRoomSerial.listen();
  delay(DELAY_PER_SLAVE);
  String read_string_f_room;
  while (fRoomSerial.available() > 0) {
    char read_char = fRoomSerial.read();
    if (read_char == '!')
    {
      read_char = fRoomSerial.read();
      while (read_char != '%')
      {
        read_string_f_room += String(read_char);
        read_char = fRoomSerial.read();
      }
      break;
    }
  }
  if (read_string_f_room.length() > 2)
  {
    send_string += read_string_f_room;
    is_data_available = true;
  }

  /************************* ATM_DOOR_MODULE ************************/
  atmSerial.listen();
  delay(DELAY_PER_SLAVE);
  String read_string_atm_door;
  while (atmSerial.available() > 0) {
    char read_char = atmSerial.read();
    if (read_char == '!')
    {
      read_char = atmSerial.read();
      while (read_char != '%')
      {
        read_string_atm_door += String(read_char);
        read_char = atmSerial.read();
      }
      break;
    }
  }
  if (read_string_atm_door.length() > 2)
  {
    if (is_data_available == true)
    {
      send_string += TERMINATE_CHAR; //previous data available. terminate with ","
    }
    send_string += read_string_atm_door;
    is_data_available = true;
  }

  /************************* ULTRASOUND_MODULE ************************/
  ultrasoundSerial.listen();
  delay(DELAY_PER_SLAVE);
  String read_string_ultrasound;
  while (ultrasoundSerial.available() > 0) {
    char read_char = ultrasoundSerial.read();
    if (read_char == '!')
    {
      read_char = ultrasoundSerial.read();
      while (read_char != '%')
      {
        read_string_ultrasound += String(read_char);
        read_char = ultrasoundSerial.read();
      }
      break;
    }
  }
  if (read_string_ultrasound.length() > 2)
  {
    if (is_data_available == true)
    {
      send_string += TERMINATE_CHAR; //previous data available. terminate with ","
    }
    send_string += read_string_ultrasound;
    is_data_available = true;
  }

  /************************* SENSOR_MODULE ************************/
  sensorSerial.listen();
  delay(DELAY_PER_SLAVE);
  String read_string_sensor;
  while (sensorSerial.available() > 0) {
    char read_char = sensorSerial.read();
    if (read_char == '!')
    {
      read_char = sensorSerial.read();
      while (read_char != '%')
      {
        read_string_sensor += String(read_char);
        read_char = sensorSerial.read();
      }
      break;
    }
  }
  if (read_string_sensor.length() > 2)
  {
    if (is_data_available == true)
    {
      send_string += TERMINATE_CHAR; //previous data available. terminate with ","
    }
    send_string += read_string_sensor;
    is_data_available = true;
  }

  /************************* SEND TO PC ************************/
  if (is_data_available == true)
  {
    send_string += END_CHAR; //add end character
    Serial.println(send_string);
    last_sent_millis = millis();
    is_data_available = false;
  }

  delay(200);
}
