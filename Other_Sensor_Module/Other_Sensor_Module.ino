#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "DHT.h"
#define DHTTYPE DHT11

#define GRID_PIN 2
#define TRIP_PIN 3
#define UPS_PIN 4
#define DHT11_SENSOR_PIN 5
#define DOOR_SENOSOR_PIN 6
#define SERIAL_TX 7
#define SERIAL_RX 8
#define MODBUS_DI 12
#define MODBUS_RO 13
#define LIGHT_1_PIN A0
#define LIGHT_2_PIN A1
#define LIGHT_3_PIN A2
#define LIGHT_4_PIN A3

#define START_CHAR String("!")
#define END_CHAR String("%")
/*#define ROOM_TEMP_BACK_KEY String("\"room_temp_back\": ")
#define B_DOOR_STATE_KEY String("\"b_door_state\": ")
#define B_DOOR_COUNT_KEY String("\"b_door_count\": ")*/
#define ROOM_TEMP_BACK_KEY String("\"rtb\": ")
#define B_DOOR_STATE_KEY String("\"bds\": ")
#define B_DOOR_COUNT_KEY String("\"bdc\": ")
#define L1_KEY String("\"l1\": ")
#define L2_KEY String("\"l2\": ")
#define L3_KEY String("\"l3\": ")
#define L4_KEY String("\"l4\": ")
#define GRID_KEY String("\"grid\": ")
#define TRIP_KEY String("\"trip\": ")
#define UPS_KEY String("\"ups\": ")

#define SEND_COUNT 20
#define SEND_DELAY 200

#define LIGHT_POW_SEND_PER_TEMP_DOOR 3 //light and temp will be sent at every 3 times per temp/door sending

#define LIGHT_READ_AVERAGE_COUNT 5
#define LIGHT_SENSOR_ZERO_VAL 511
#define LIGHT_SENSOR_THRESHOLD 20 //0 current output 511 to analogRead. check the ABS difference

#define B_DOOR_COUNT_EEPROM_ADDRESS 10

#define NEGLIGIBLE_TEMP_DIFF 1.00  //if the temperature difference between old and current are greater than this, push the new value
#define TEMP_READ_INTERVAL 5000 //temperature read interval in miliseconds

//SoftwareSerial mySerial(SERIAL_RX, SERIAL_TX); // RX, TX
SoftwareSerial mySerial(MODBUS_RO, MODBUS_DI); // RX, TX

DHT dht(DHT11_SENSOR_PIN, DHTTYPE);

bool read_grid_state;
bool read_trip_state;
bool read_ups_state;
bool read_b_door_state;
float read_temperature;
uint16_t b_door_count;
bool read_light_1;
bool read_light_2;
bool read_light_3;
bool read_light_4;

bool old_read_grid_state;
bool old_read_trip_state;
bool old_read_ups_state;
bool old_read_b_door_state;
float old_read_temperature;
bool old_read_light_1;
bool old_read_light_2;
bool old_read_light_3;
bool old_read_light_4;

bool need_to_send_temp_door = false;
bool need_to_send_light = false;
bool need_to_send_power = false;

unsigned long last_temp_read_millis;

uint8_t temp_door_send_count = 0;

void setup() {
  Serial.begin(115200);
  mySerial.begin(9600);
  dht.begin();

  pinMode(GRID_PIN, INPUT);
  pinMode(TRIP_PIN, INPUT);
  pinMode(UPS_PIN, INPUT);
  pinMode(DOOR_SENOSOR_PIN, INPUT); //pulldown to NO switch, when closed: read 1
  pinMode(LIGHT_1_PIN, INPUT);
  pinMode(LIGHT_2_PIN, INPUT);
  pinMode(LIGHT_3_PIN, INPUT);
  pinMode(LIGHT_4_PIN, INPUT);

  delay(6000);

  read_grid_state = digitalRead(GRID_PIN);
  read_trip_state = digitalRead(TRIP_PIN);
  read_ups_state = digitalRead(UPS_PIN);
  read_b_door_state = (bool)(!digitalRead(DOOR_SENOSOR_PIN)); //inverted the signal to read 1 when door is open
  EEPROM.get(B_DOOR_COUNT_EEPROM_ADDRESS, b_door_count); //get backdoor value from EEPROM
  read_temperature = dht.readTemperature();
  last_temp_read_millis = millis();
  read_light_1 = isLightOn(LIGHT_1_PIN);
  read_light_2 = isLightOn(LIGHT_2_PIN);
  read_light_3 = isLightOn(LIGHT_3_PIN);
  read_light_4 = isLightOn(LIGHT_4_PIN);


  old_read_grid_state = read_grid_state;
  old_read_trip_state = read_trip_state;
  old_read_ups_state  = read_ups_state;
  old_read_b_door_state = read_b_door_state;
  old_read_temperature = read_temperature;
  old_read_light_1 = read_light_1;
  old_read_light_2 = read_light_2;
  old_read_light_3 = read_light_3;
  old_read_light_4 = read_light_4;

  need_to_send_temp_door = true; //send for the first time
  need_to_send_light = true;
  need_to_send_power = true;
}

void loop() {
  if ((millis() - last_temp_read_millis) > TEMP_READ_INTERVAL)
  {
    read_temperature = dht.readTemperature();
    last_temp_read_millis = millis();

    if (abs(read_temperature - old_read_temperature) >= NEGLIGIBLE_TEMP_DIFF)
    {
      need_to_send_temp_door = true; //temperature difference detected
      old_read_temperature = read_temperature;
    }

    read_grid_state = digitalRead(GRID_PIN);
    read_trip_state = digitalRead(TRIP_PIN);
    read_ups_state = digitalRead(UPS_PIN);
    read_light_1 = isLightOn(LIGHT_1_PIN);
    read_light_2 = isLightOn(LIGHT_2_PIN);
    read_light_3 = isLightOn(LIGHT_3_PIN);
    read_light_4 = isLightOn(LIGHT_4_PIN);

    if ((read_light_1 != old_read_light_1) ||
        (read_light_2 != old_read_light_2) ||
        (read_light_3 != old_read_light_3) ||
        (read_light_4 != old_read_light_4))
    {
      need_to_send_light = true;
      old_read_light_1    = read_light_1;
      old_read_light_2    = read_light_2;
      old_read_light_3    = read_light_3;
      old_read_light_4    = read_light_4;
    }

    if ((read_grid_state != old_read_grid_state) ||
        (read_trip_state != old_read_trip_state) ||
        (read_ups_state != old_read_ups_state))
    {
      need_to_send_power = true;
      old_read_grid_state = read_grid_state;
      old_read_trip_state = read_trip_state;
      old_read_ups_state  = read_ups_state;
    }
  }

  read_b_door_state = (bool)(!digitalRead(DOOR_SENOSOR_PIN)); //inverted the signal to read 1 when door is open
  if ((read_b_door_state == true) && (old_read_b_door_state == false))
  {
    //door open detected
    b_door_count++; //increment back door open count
    EEPROM.put(B_DOOR_COUNT_EEPROM_ADDRESS, b_door_count); //get backdoor value from EEPROM
    need_to_send_temp_door = true;
    old_read_b_door_state = read_b_door_state;
  }
  else if ((read_b_door_state == false) && (old_read_b_door_state == true))
  {
    //door close detected
    need_to_send_temp_door = true;
    old_read_b_door_state = read_b_door_state;
  }

  if (need_to_send_temp_door == true)
  {
    String send_string = START_CHAR +
                         ROOM_TEMP_BACK_KEY + String(read_temperature, 2) + ", " +
                         B_DOOR_STATE_KEY + String((int)read_b_door_state) + ", " +
                         B_DOOR_COUNT_KEY +  String(b_door_count) +
                         END_CHAR;

    Serial.println(send_string);
    for (int i = 0; i < SEND_COUNT; i++)
    {
      mySerial.println(send_string);
      delay(SEND_DELAY);
    }
    need_to_send_temp_door = false; //reset sent flag

    temp_door_send_count++;
  }

  if(temp_door_send_count >= LIGHT_POW_SEND_PER_TEMP_DOOR)
  {
    need_to_send_light = true;
    need_to_send_power = true;
    temp_door_send_count = 0;
  }

  if (need_to_send_light == true)
  {
    String send_string = START_CHAR +
                         L1_KEY +  String((int)read_light_1) + ", " +
                         L2_KEY +  String((int)read_light_2) + ", " +
                         L3_KEY +  String((int)read_light_3) + ", " +
                         L4_KEY +  String((int)read_light_4) +
                         END_CHAR;

    Serial.println(send_string);
    for (int i = 0; i < SEND_COUNT; i++)
    {
      mySerial.println(send_string);
      delay(SEND_DELAY);
    }
    need_to_send_light = false; //reset sent flag
  }

  if (need_to_send_power == true)
  {
    String send_string = START_CHAR +
                         GRID_KEY +  String((int)read_grid_state) + ", " +
                         TRIP_KEY +  String((int)read_trip_state) + ", " +
                         UPS_KEY +  String((int)read_ups_state) +
                         END_CHAR;

    Serial.println(send_string);
    for (int i = 0; i < SEND_COUNT; i++)
    {
      mySerial.println(send_string);
      delay(SEND_DELAY);
    }
    need_to_send_power = false; //reset sent flag
  }

  delay(1000); //loop delay
}

bool isLightOn(int pin)
{
  int analog_val = 0;
  for (int i = 0; i < LIGHT_READ_AVERAGE_COUNT; i++)
  {
    analog_val += analogRead(pin);
    delay(10);
  }
  analog_val = (int)(analog_val / LIGHT_READ_AVERAGE_COUNT); //taking the average

  if (abs(analog_val - LIGHT_SENSOR_ZERO_VAL) >= LIGHT_SENSOR_THRESHOLD)
  {
    return true;
  }
  else
  {
    return false;
  }
}
